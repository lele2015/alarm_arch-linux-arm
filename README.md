# ALARM_ArchLinuxARM LiveCD

#### 介绍

##### 关于 ALARM
---
ALARM(Arch Linux ARM) 是针对 ARM 架构移植的 Arch Linux 发行版。
官方主页
https://www.archlinuxarm.org/

论坛
https://archlinuxarm.org/forum/

Wiki:
https://archlinuxarm.org/wiki

##### 为什么要做这个ALARM 的LiveCD
---
ALARM目前主要面向arm开发板，提供img镜像格式的安装文件，通过SD卡烧录等方式进行安装。
参考ArchWiki,使用arch-bootstrap基于其他linux发行版安装archlinux（https://wiki.archlinux.org/title/Install_Arch_Linux_from_existing_Linux_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87)），
安装需要参考（https://wiki.archlinux.org/title/Installation_guide_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87)），对于新手不够友好，比较繁琐。

为了能在基于arm架构的国产CPU（飞腾FT-2000/4,飞腾D2000,鲲鹏920等）电脑主机上顺利安装archlinux-arm,
使用archiso-aarch64(https://github.com/archlinux-jerry/archiso-aarch64)工具创建ALARM-LiveCD,并在LiveCD里内置calamares installer（https://github.com/calamares/calamares）,实现系统图形化安装。

#### 软件架构
支持的arm架构包括：

AArch64


#### 安装教程

1.  下载已经做好的LiveCD（KDE桌面）。链接：https://pan.baidu.com/s/1AhsPeAiTbtvQcP6V_-u5qw?pwd=1ik1 提取码：1ik1 
2.  使用DD命令写入到U盘
3.  在arm国产电脑主机上插入U盘，选择U盘启动，然后进行安装。安装过程可参考B站UP主视频（我梦见凤鸟飞腾）


#### 参与贡献
参与讨论和共建
Q群：750853172
B站关注Up主：我梦见凤鸟飞腾

1.  Fork 本仓库
2.  新建分支
3.  提交代码
4.  新建 Pull Request


