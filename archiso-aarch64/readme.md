 
使用方法：
1、克隆到本地后，进入目录
2、执行以下代码

```
sudo ./archiso/mkarchiso -v config/iso配置文件目录
```
+基础镜像：
```
sudo ./archiso/mkarchiso -v config/releng
```
+xfce镜像
```
sudo ./archiso/mkarchiso -v config/xfce-test
```
+plasma镜像
```
sudo ./archiso/mkarchiso -v config/kde-test
```
+cutefish镜像
```
sudo ./archiso/mkarchiso -v config/cutefish-test
```
3.运行报错，按照提示安装缺失的依赖包即可
4.生成的iso文件在out目录下
5.如果想重新生成iso，在目录下执行脚本

```
sudo ./cleanup.sh
```
