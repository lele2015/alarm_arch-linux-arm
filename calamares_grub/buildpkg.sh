#!/bin/bash
WORKDIR=$(pwd)
FROM=$(pwd)/conf
DEST=$WORKDIR/src/calamares-3.2.61

echo "*** calamares package build start ***" 
# clean
rm -rf pkg
rm -rf src
echo "- make calamares clean done!"

# extract source
makepkg -o
echo "extract source done !"

# copy config files 
/bin/cp -rf $FROM/settings.conf $DEST
/bin/cp -rf $FROM/branding $DEST/src
/bin/cp -rf $FROM/bootloader.conf $DEST/src/modules/bootloader/
/bin/cp -rf $FROM/initcpio.conf $DEST/src/modules/initcpio/
/bin/cp -rf $FROM/mount.conf $DEST/src/modules/mount/
/bin/cp -rf $FROM/packages.conf $DEST/src/modules/packages/
/bin/cp -rf $FROM/removeuser.conf $DEST/src/modules/removeuser/
/bin/cp -rf $FROM/shellprocess-remove-livecd.conf $DEST/src/modules/shellprocess/
/bin/cp -rf $FROM/unpackfs.conf $DEST/src/modules/unpackfs/
/bin/cp -rf $FROM/users.conf $DEST/src/modules/users/
/bin/cp -rf $FROM/welcome.conf $DEST/src/modules/welcome/
/bin/cp -rf $FROM/partition.conf $DEST/src/modules/partition/
echo "copy config files done !"

# makepkg
makepkg -e -f
